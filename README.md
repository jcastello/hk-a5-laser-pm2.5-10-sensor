# HK-A5 Laser PM2.5-10 Sensor

Classe pour le HK-A5 Laser PM2.5/10 Sensor (SEN0177)

- https://www.gotronic.fr/art-capteur-de-poussieres-gravity-sen0177-24147.htm

- https://www.dfrobot.com/wiki/index.php/PM2.5_laser_dust_sensor_SKU:SEN0177

- https://github.com/Arduinolibrary/DFRobot_PM2.5_Sensor_module/blob/master/HK-A5%20Laser%20PM2.5%20Sensor%20V1.0.pdf
