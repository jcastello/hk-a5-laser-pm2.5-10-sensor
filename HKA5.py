import serial
import time


"""
Classe pour le HK-A5 Laser PM2.5/10 Sensor

https://www.gotronic.fr/art-capteur-de-poussieres-gravity-sen0177-24147.htm
https://www.dfrobot.com/wiki/index.php/PM2.5_laser_dust_sensor_SKU:SEN0177
https://github.com/Arduinolibrary/DFRobot_PM2.5_Sensor_module/blob/master/HK-A5%20Laser%20PM2.5%20Sensor%20V1.0.pdf

Trame: 0x42 (66) + 0x4D (77) + 30 bytes
len = 1C (28) + 2 + 2 = 32 bytes
"""


class HKA5:

    def __init__(self,  port_name="/dev/ttyS0", debug=True):
        self.FRAME_LEN = 31
        self.START1 = 0x42
        self.START2 = 0x4D
        self.checksum = False 
        self.buffer = b'' #bytes (array d'entiers)
        self.debug = debug
        try:
            with serial.Serial() as self.serial_port:
                self.serial_port.port = port_name
                self.serial_port.baudrate = 9600
                self.serial_port.timout = 2.0       
            self.serial_port.open()    
        except serial.serialutil.SerialException as e:
            print(e)
            sys.exit()      
      
    def read(self):
        try:
            if self.serial_port.inWaiting() >= (self.FRAME_LEN + 1):
                c = self.serial_port.read(1) 
                if c[0] == self.START1:     
                    self.buffer = self.serial_port.read(self.FRAME_LEN)
                    self.checksum = self.check()
            else:
                self.checksum = False
                if self.debug:
                    print("Erreur de lecture sur la liaison série")            
        except:
            self.checksum = False
            if self.debug:
                print('Erreur de connexion au port série')
           
    def check(self):
        check_calc = self.START1
        for c in self.buffer[0:29:1]:
            check_calc += c

        if check_calc == (self.buffer[29] << 8) + self.buffer[30]:
            return True
        else:
            return False
        
    def getPM01(self):
        if self.checksum:
            return (self.buffer[3] << 8) + self.buffer[4]
        else:
            return -1

    def getPM2_5(self):
        if self.checksum:
           return (self.buffer[5] << 8) + self.buffer[6]
        else:
            return -1

    def getPM10(self):
        if self.checksum:
            return (self.buffer[7] << 8) + self.buffer[8]
        else:
            return -1

    def getNbMore0_3(self):
        if self.checksum:
            return (self.buffer[15] << 8) + self.buffer[16]
        else:
            return -1

    def getNbMore0_5(self):
        if self.checksum:
            return (self.buffer[17] << 8) + self.buffer[18]
        else:
            return -1

    def getNbMore1_0(self):
        if self.checksum:
            return (self.buffer[19] << 8) + self.buffer[20]
        else:
            return -1

    def getNbMore2_5(self):
        if self.checksum:
            return (self.buffer[21] << 8) + self.buffer[22]
        else:
            return -1

    def getNbMore5_0(self):
        if self.checksum:
            return (self.buffer[23] << 8) + self.buffer[24]
        else:
            return -1

    def getNbMore10_0(self):
        if self.checksum:
            return (self.buffer[25] << 8) + self.buffer[26]
        else:
            return -1


if __name__ == '__main__':
    print('Starting...')
    sensor = HKA5(port_name="/dev/ttyS0", debug=True)

    while 1:
        sensor.read()
        print('PM01= {} ug/m3'.format(sensor.getPM01()))
        print('PM2.5= {} ug/m3'.format(sensor.getPM2_5()))
        print('PM10= {} ug/m3'.format(sensor.getPM10()))
        print('Number of particules more than 0.3um in 0.1 liters= {}'.format(sensor.getNbMore0_3()))
        print('Number of particules more than 0.5um in 0.1 liters= {}'.format(sensor.getNbMore0_5()))
        print('Number of particules more than 1.0um in 0.1 liters= {}'.format(sensor.getNbMore1_0()))
        print('Number of particules more than 2.5um in 0.1 liters= {}'.format(sensor.getNbMore2_5()))
        print('Number of particules more than 5.0um in 0.1 liters= {}'.format(sensor.getNbMore5_0()))
        print('Number of particules more than 10.0um in 0.1 liters= {}\n'.format(sensor.getNbMore10_0()))
        time.sleep(5.0)
